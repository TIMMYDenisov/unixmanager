#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

from tkinter import filedialog, messagebox
import peewee
from peewee import *
import tkinter as tk
from tkinter import ttk
import os
import sqlite3
import subprocess
import datetime
from hurry.filesize import size
import shutil

db = SqliteDatabase('fileManager.db')


class User(Model):
    login = CharField()
    password = CharField()

    class Meta:
        database = db


class MainWindow(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.style = ttk.Style()
        self.folder = tk.PhotoImage(file='assets/folder_icon.png')
        self.file = tk.PhotoImage(file='assets/file_icon.png')
        self.new_file = tk.PhotoImage(file='assets/new_file.png')
        self.new_folder = tk.PhotoImage(file='assets/new_folder.png')
        self.main_menu = tk.Menu(root)
        self.help_menu = tk.Menu(self.main_menu, tearoff=0)
        self.edit_menu = tk.Menu(self.main_menu, tearoff=0)
        self.file_menu = tk.Menu(self.main_menu, tearoff=0)
        self.file_tree = ttk.Treeview(root, height=50, columns=(
            'File_name', 'Size', 'Creation_Date'))
        self.scroll = ttk.Scrollbar(
            root, orient='vertical', command=self.file_tree.yview)
        self.right_arrow = tk.PhotoImage(file='assets/right-arrow24.png')
        self.left_arrow = tk.PhotoImage(file='assets/left-arrow24.png')
        self.current_path = os.path.abspath('/home/timmy/')
        self.path_str = tk.StringVar()
        self.path_label = tk.Label()
        self.buffer = {}
        self.next_path = []
        self.prev_path = []
        self.reverse = False
        self.active_user = None
        self.isVisible = False
        self.init_main()
        ChildAuthorization()

    def init_main(self):
        toolbar = tk.Frame(bg='#555', bd=2)
        root.config(menu=self.main_menu)

        self.file_menu.add_command(
            label='New file (Ctrl+N)', command=self.create_new_file)
        self.file_menu.add_command(
            label='New folder', command=self.create_new_folder)
        self.file_menu.add_command(
            label='Show hidden items (Ctrl+H)', command=self.show_hidden)

        self.edit_menu.add_command(label='Rename (F2)', command=self.rename)
        self.edit_menu.add_command(label='Remove (Del)', command=self.remove)

        self.help_menu.add_command(label='About', command=self.about_wind)

        self.main_menu.add_cascade(label='File', menu=self.file_menu)
        self.main_menu.add_cascade(label='Edit', menu=self.edit_menu)
        self.main_menu.add_cascade(label='Help', menu=self.help_menu)
        self.main_menu.add_command(
            label='UserManager', command=self.user_managment)

        path_label = tk.Entry(toolbar, width=100, textvariable=self.path_str)
        self.path_str.set(self.current_path)
        self.path_str.trace('w', lambda name, index, mode,
                            path_str=self.path_str: self.change_path_string(path_str))

        toolbar.pack(side=tk.TOP, fill=tk.X)

        btn_back = tk.Button(toolbar,
                             background='#555',
                             foreground='#ccc',
                             highlightcolor='#555',
                             activebackground='#555',
                             font='16',
                             image=self.left_arrow,
                             command=self.prev_address)

        btn_forward = tk.Button(toolbar,
                                background='#555',
                                foreground='#ccc',
                                highlightcolor='#555',
                                activebackground='#555',
                                font='16',
                                image=self.right_arrow,
                                command=self.next_address)

        btn_new_file = tk.Button(toolbar,
                                 background='#555',
                                 foreground='#ccc',
                                 highlightcolor='#555',
                                 activebackground='#555',
                                 font='16',
                                 image=self.new_file,
                                 command=self.create_new_file)
        btn_new_folder = tk.Button(toolbar,
                                   background='#555',
                                   foreground='#ccc',
                                   highlightcolor='#555',
                                   activebackground='#555',
                                   font='16',
                                   image=self.new_folder,
                                   command=self.create_new_folder)

        btn_back.pack(side=tk.LEFT)
        btn_forward.pack(side=tk.LEFT)
        btn_new_file.pack(side=tk.LEFT)
        btn_new_folder.pack(side=tk.LEFT)
        path_label.pack(side=tk.RIGHT)
        self.scroll.place(x=733+200+2, y=55, height=290+200)
        self.style.configure('Treeview', rowheight=30)
        # self.file_tree['show'] = 'headings'
        self.file_tree.column('#0', width=50)
        self.file_tree.heading('#1', text='File name', command=lambda: self.file_sort(
            'File_name', self.reverse))
        self.file_tree.column('#1', minwidth=200, width=600)
        self.file_tree.heading(
            '#2', text='File size', command=lambda: self.file_sort('Size', self.reverse))
        self.file_tree.column('#2', minwidth=75, width=100)
        self.file_tree.heading('#3', text='Last update', command=lambda: self.file_sort(
            'Creation_Date', self.reverse))
        self.file_tree.column('#3', minwidth=150, width=200)
        self.file_tree.bind("<Double-Button-1>", self.open_file)
        self.file_tree.configure(yscrollcommand=self.scroll.set)
        self.file_tree.bind('<Control-n>', self.create_new_file)
        self.file_tree.bind('<Control-f>', self.create_new_folder)
        self.file_tree.bind('<Control-h>', self.show_hidden)
        self.file_tree.bind('<F2>', self.rename)
        self.file_tree.bind('<Delete>', self.remove)
        self.file_tree.bind('<Control-c>', self.copy_cut)
        self.file_tree.bind('<Control-v>', self.paste)
        self.file_tree.bind('<Control-x>', self.copy_cut)
        self.file_tree.tag_configure('odd', background='#E8E8E8')
        self.file_tree.tag_configure('even', background='#DFDFDF')
        self.file_tree.pack(side=tk.LEFT)

    def copy_cut(self, event):
        selection = self.file_tree.focus()
        self.buffer.clear()
        if (event.keysym == 'x'):
            self.buffer['cut'] = self.current_path + '/' + \
                self.file_tree.item(selection).get('values')[0]
        else:
            self.buffer['copy'] = self.current_path + '/' + \
                self.file_tree.item(selection).get('values')[0]

    def paste(self, event):
        key, value = list(self.buffer.items())[0]
        if (key == 'copy'):
            shutil.copy(value, self.current_path + '/')
        else:
            shutil.move(value, self.current_path + '/')
        self.show_files()

    def get_size(self, folder_path=None):
        curr_size = 0
        for root, dirs, files in os.walk(folder_path, topdown=False):
            for f in files:
                try:
                    curr_size += os.stat(root + '/' + f).st_size
                except FileNotFoundError:
                    pass
        return size(curr_size)

    def change_path_string(self, path_str):
        self.current_path = path_str.get()
        self.show_files()

    def next_address(self):
        if self.next_path:
            self.prev_path.append(self.current_path)
            self.path_str.set(self.next_path.pop())

    def prev_address(self):
        if self.current_path != '/' and self.prev_path:
            self.next_path.append(self.current_path)
            self.path_str.set(self.prev_path.pop())

    def check_user(self):
        if not self.active_user.password:
            self.main_menu.entryconfig('File', state='disabled')
            self.main_menu.entryconfig('Edit', state='disabled')
        if self.active_user.login != 'admin':
            self.main_menu.entryconfig('UserManager', state='disabled')
        self.show_files()

    def file_sort(self, col, reverse):
        l = [(self.file_tree.set(k, col), k)
             for k in self.file_tree.get_children('')]
        l.sort(key=lambda t: t[0], reverse=reverse)
        self.reverse = not reverse

        for index, (val, k) in enumerate(l):
            self.file_tree.move(k, '', index)

    def open_file(self, event):
        selection = self.file_tree.focus()
        value = self.file_tree.item(selection).get('values')[0]
        if os.path.isdir(self.current_path + '/' + str(value)):
            self.change_dir(value)
        else:
            cur_file = self.current_path + '/' + value
            subprocess.call(['xdg-open', cur_file])
        self.show_files()

    def change_dir(self, new_path):
        self.prev_path.append(self.current_path)
        self.path_str.set(self.current_path + '/' + str(new_path))

    @staticmethod
    def create_new_file(event=None):
        NewFileWindow('File')

    @staticmethod
    def create_new_folder(event=None):
        NewFileWindow('Folder')

    def show_files(self):
        self.file_tree.delete(*self.file_tree.get_children())
        if os.path.exists(self.current_path):
            ftmp = os.listdir(self.current_path + '/')
            if not self.isVisible:
                for file in ftmp[::-1]:
                    if file.startswith('.'):
                        ftmp.pop(ftmp.index(file))
            for i, item in enumerate(ftmp):
                statbuf = os.stat(self.current_path + '/' + item)
                file_size = size(statbuf.st_size) if not os.path.isdir(
                    self.current_path + '/' + item) else self.get_size(self.current_path + '/' + item)
                lastUpdate = datetime.datetime.fromtimestamp(
                    statbuf.st_mtime).strftime('%d:%m:%Y %H.%M')
                cur_tag = 'odd' if i % 2 == 0 else 'even'
                image = self.file if os.path.isfile(
                    self.current_path + '/' + item) else self.folder
                self.file_tree.insert('', 'end', image=image, values=(
                    item, file_size, lastUpdate), tags=(cur_tag,))

    def show_hidden(self, event=None):
        self.isVisible = not self.isVisible
        self.show_files()

    def rename(self, event=None):
        selection = self.file_tree.selection()[0]
        value = str(self.file_tree.item(selection).get('values')[0])
        RenameFileWindow(value, self.current_path)

    def remove(self, event=None):
        selection = self.file_tree.selection()[0]
        os.remove(self.current_path + '/' +
                  str(self.file_tree.item(selection).get('values')[0]))
        self.file_tree.delete(selection)

    @staticmethod
    def about_wind():
        messagebox.showinfo(
            message="This application allows you to work with files on your PC.", title='About')

    @staticmethod
    def open_property(name):
        ChildPropertyWindow(name)

    @staticmethod
    def user_managment():
        UserManagerWindow()


class UserManagerWindow(tk.Toplevel):
    def __init__(self):
        super().__init__(root)
        self.entry_login = None
        self.entry_password = None
        self.label_login = None
        self.label_password = None
        self.create_button = None
        self.remove_button = None
        self.exit_button = None
        self.isAdmin = tk.IntVar()
        self.list_users = ttk.Treeview(self, columns=('Login', 'Password'))
        self.i = 0
        self.init_child()
        self.init_tree()

    def init_child(self):
        self.title('User manager')
        self.geometry('600x540+300+200')
        self.resizable(False, False)
        self.list_users.heading('#0', text='Number')
        self.list_users.heading('#1', text='Login')
        self.list_users.heading('#2', text='Password')
        self.list_users.column('#0', stretch=tk.YES)
        self.list_users.column('#2', stretch=tk.YES)
        self.list_users.column('#1', stretch=tk.YES)
        self.list_users.grid(row=4, columnspan=4, sticky='nsew')

        self.label_login = tk.Label(self, text='Login:')
        self.entry_login = tk.Entry(self)
        self.label_login.grid(row=0, column=0, sticky=tk.W)
        self.entry_login.grid(row=0, column=1)

        self.label_password = tk.Label(self, text="Password:")
        self.entry_password = tk.Entry(self, textvariable='password', show='*')
        self.label_password.grid(row=1, column=0, sticky=tk.W)
        self.entry_password.grid(row=1, column=1)

        self.create_button = tk.Button(
            self, text="Create user", command=self.create_action)
        self.create_button.grid(row=3, column=1, sticky=tk.W)
        self.remove_button = tk.Button(
            self, text='Remove user', command=self.delete_action)
        self.remove_button.grid(row=3, column=2, sticky=tk.W)
        self.exit_button = tk.Button(
            self, text="Exit", command=self.close_action)
        self.exit_button.grid(row=3, column=3)

    def init_tree(self):
        self.i = 0
        models = User.select()
        for user in models:
            self.i += 1
            self.list_users.insert('', 'end', text='{}'.format(
                str(self.i)), values=(user.login, user.password))

    def create_action(self):
        try:
            user = User.select().where(User.login == self.entry_login.get()).get()
        except peewee.DoesNotExist as de:
            user = User(login=self.entry_login.get(),
                        password=self.entry_password.get())
            user.save()
        self.i += 1
        self.list_users.insert('', 'end', text='{}'.format(str(self.i)), values=(self.entry_login.get(),
                                                                                 self.entry_password.get()))

    def delete_action(self):
        curItem = self.list_users.focus()
        user = User.get(User.login == self.list_users.item(
            curItem).get('values')[0])
        user.delete_instance()
        self.list_users.delete(*self.list_users.get_children())
        self.init_tree()

    def close_action(self):
        self.destroy()


class RenameFileWindow(tk.Toplevel):
    def __init__(self, file_name, cur_path):
        super().__init__(root)
        self.entry_name = None
        self.label_name = None
        self.file_name = file_name
        self.cur_path = cur_path
        self.init_child()

    def init_child(self):
        self.title('Rename {}'.format(self.file_name))
        self.geometry('400x175')
        self.resizable(False, False)
        self.label_name = tk.Label(self, text='New name: ')
        self.entry_name = tk.Entry(self)
        btn_create = tk.Button(self, bg='white', font='16',
                               text='Create', command=self.rename_action)
        btn_cancel = tk.Button(self, bg='white', font='16',
                               text='Cancel', command=self.cancel_action)
        self.label_name.place(x=25, y=50)
        self.entry_name.place(x=100, y=50)
        btn_cancel.place(x=300, y=100)
        btn_create.place(x=25, y=100)

    def rename_action(self):
        os.rename(self.cur_path + '/' + self.file_name,
                  self.cur_path + '/' + self.entry_name.get())
        self.destroy()
        app.show_files()

    def cancel_action(self):
        self.destroy()


class NewFileWindow(tk.Toplevel):
    def __init__(self, obj_type):
        super().__init__(root)
        self.entry_name = None
        self.label_name = None
        self.obj_type = obj_type
        self.init_child()

    def init_child(self):
        self.title('Create new {}'.format(self.obj_type))
        self.geometry('400x175')
        self.resizable(False, False)
        self.label_name = tk.Label(
            self, text='{} name: '.format(self.obj_type))
        self.entry_name = tk.Entry(self)
        btn_create = tk.Button(self, bg='white', font='16',
                               text='Create', command=self.create_action)
        btn_cancel = tk.Button(self, bg='white', font='16',
                               text='Cancel', command=self.cancel_action)
        self.label_name.place(x=25, y=50)
        self.entry_name.place(x=100, y=50)
        btn_cancel.place(x=300, y=100)
        btn_create.place(x=25, y=100)

    def create_action(self):
        if self.obj_type == 'File':
            path = app.current_path + '/' + self.entry_name.get()
            f = open(path, 'w')
            f.close()
            self.destroy()
            app.show_files()
        else:
            path = app.current_path + '/' + self.entry_name.get() + '/'
            os.mkdir(path)
            self.destroy()
            app.show_files()

    def cancel_action(self):
        self.destroy()


class ChildPropertyWindow(tk.Toplevel):
    def __init__(self, name):
        super().__init__(root)
        self.init_child(name)

    def init_child(self, name):
        self.title('Property "{}"'.format(name))
        self.geometry('400x200+300+200')
        self.resizable(False, False)
        self.grab_set()
        self.focus_set()


class ChildAuthorization(tk.Toplevel):
    def __init__(self):
        super().__init__(root)
        self.entry_login = None
        self.entry_password = None
        self.label_login = None
        self.label_password = None
        self.var = tk.IntVar()
        self.isVisible = True
        self.init_child()

    def init_child(self):
        self.title('Authorization')
        self.geometry('400x200+400+300')
        self.resizable(False, False)
        self.entry_login = tk.Entry(self)
        self.entry_login.place(x=150, y=50)
        self.entry_password = tk.Entry(self, textvariable='password', show='*')
        self.entry_password.place(x=150, y=80)
        self.label_login = tk.Label(self, text='Login:')
        self.label_login.place(x=50, y=50)
        self.label_password = tk.Label(self, text='Password:')
        self.label_password.place(x=50, y=80)
        login_check = tk.Checkbutton(self, text='Guest', variable=self.var)
        login_check.place(x=50, y=115)
        login_check.bind('<Button-1>', self.change)
        btn_create = tk.Button(self, bg='white', font='16',
                               text='Login', command=self.authorization_action)
        btn_cancel = tk.Button(self, bg='white', font='16',
                               text='Exit', command=self.cancel_action)
        btn_create.place(x=50, y=150)
        btn_cancel.place(x=300, y=150)
        self.grab_set()
        self.focus_set()
        btn_cancel.bind('<Button-1>', self.cancel_action)

    def change(self, event):
        if self.isVisible:
            self.entry_password.config(state='disabled')
            self.isVisible = False
        else:
            self.entry_password.config(state='normal')
            self.isVisible = True

    @staticmethod
    def cancel_action(event):
        root.destroy()

    def authorization_action(self):
        if not self.isVisible:
            user = User(login=self.entry_login.get(), password=None)
            app.active_user = user
            root.title('Unix manager Guest {}'.format(user.login))
            self.destroy()
            app.check_user()
        else:
            if len(self.entry_login.get()) < 5:
                messagebox.showerror('Validation error',
                                     'Login is too short(< 5 symbols)!')
            elif len(self.entry_password.get()) < 8:
                messagebox.showerror('Validation error',
                                     'Password is too short(< 8 symbols)!')
            else:
                try:
                    user = User.select().where(User.login == self.entry_login.get()).get()
                except peewee.DoesNotExist as de:
                    user = User(login=self.entry_login.get(),
                                password=self.entry_password.get())
                    user.save()
                if user and user.password != self.entry_password.get():
                    messagebox.showerror(
                        'Login error', 'Wrong login or password!')
                if user and user.password == self.entry_password.get():
                    app.active_user = user
                    root.title('Unix manager ({})'.format(user.login))
                    self.destroy()
                    app.check_user()


if __name__ == '__main__':
    db.connect()
    if not db.table_exists('user'):
        db.create_tables([User])
        admin = User(login='admin', password='password')
        admin.save()
    root = tk.Tk()
    app = MainWindow(root)
    app.pack()
    root.title('Unix manager')
    root.geometry('950x550+300+200')
    root.tk.call('wm', 'iconphoto', root._w, tk.PhotoImage(file=os.path.join(
        '/home/timmy/PycharmProjects/Unix_manager/assets/file_manager_48.png')))
    root.resizable(False, False)
    root.mainloop()
